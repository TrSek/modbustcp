//---------------------------------------------------------------------------
// MODBUS rountine
//---------------------------------------------------------------------------
// Author:	Zdeno Sekerak
// Date:	23.02.2011
//---------------------------------------------------------------------------

#ifndef MODBUSH
#define MODBUSH
//---------------------------------------------------------------------------
#define MODBUS_COIL_OFFSET             1
#define MODBUS_DISCRETE_OFFSET     10001
#define MODBUS_INPUT_OFFSET        30001
#define MODBUS_HOLDING_OFFSET      40001

#define m_OK                0x00L
#define m_NoResponse        0x01L
#define m_IlegFunc          0x02L
#define m_IlegDataAdr       0x04L            // pristup na spatnou adresu nebo do zaheslovane oblasti
#define m_IlegDataVal       0x08L            // pokus o odemceni hesla a heslo bylo zadano chybne, nebo hodnota mimo meze
#define m_IlegDevBusy       0x10L            // zarizeni je zaneprazdneno, pozadavek je nutno zopakovat
#define m_IlegSlaveFail     0x20L            // kriticka chyba zarizeni - chybi mapa ModBusu nebo parametry
#define m_Action            0x40L
#define m_Wait              0x80L            // ceka na odpoved a odpovida - tunel protokolu ELGAS, komunikace po interni sbernici

#define SWAP_SHORT(w) ((unsigned short)((unsigned short)(w) >> 8) | (unsigned short)((unsigned short)(w) << 8))
#define SWAP_LONG(l)  ((((unsigned long)(l) & 0xFF000000) >> 24) | (((unsigned long)(l) & 0x00FF0000) >> 8) | (((unsigned long)(l) & 0x0000FF00) << 8) | (((unsigned long)(l) & 0x000000FF) << 24))

#define HEAD_LEN(x)    (sizeof(x->TransID)+sizeof(x->Protocol)+sizeof(x->Length))

/* Exported types ------------------------------------------------------------*/
typedef enum {
  MODBUS_READ_COIL       = 0x01,   // read coil status
  MODBUS_READ_DISCRETE   = 0x02,   // read input status
  MODBUS_READ_HOLD_REG   = 0x03,   // read holding registers
  MODBUS_READ_INPUT_REG  = 0x04,   // read input registers
  MODBUS_FORCE_COIL      = 0x05,   // force single coil
  MODBUS_WRITE_REGISTER  = 0x10,   // write holding registers
} enumModFunct;

typedef struct
{
    unsigned short TransID;
    unsigned short Protocol;
    unsigned short Length;
    unsigned char  Unit;
    enumModFunct   Funct;
    unsigned short Address;
    unsigned short Quantity;
    unsigned char  Data[];
}
TCP_MODBUS;

//---------------------------------------------------------------------------
#endif
