//---------------------------------------------------------------------------

#pragma hdrstop
#include <vcl.h>
#include <vcl\Clipbrd.hpp>
#include <iostream.h>
#include <fstream.h>
#include <IniFiles.hpp>
#include <DateUtils.hpp>

#include "modbus.h"
#include "ModbusTCPUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "trayicon"
#pragma link "Log"
#pragma resource "*.dfm"
TForm1 *Form1;
const unsigned long C_MONTH[]  = {0,31,59,90,120,151,181,212,243,273,304,334,365,0};
const unsigned long C_MONTHP[] = {0,31,60,91,121,152,182,213,244,274,305,335,366,0};

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
    LabelCopyright->Caption = LabelCopyright->Caption + MyGetApplicationVersion();
    readParamFile(ChangeFileExt( Application->ExeName, ".ini" ));
    ServerSocket->Active = true;
    commands = new TStringList();

    ButtonMakro1->Caption = LoadMakroName("makro1.mrk", ButtonMakro1->Caption);
    ButtonMakro2->Caption = LoadMakroName("makro2.mrk", ButtonMakro2->Caption);
    ButtonMakro3->Caption = LoadMakroName("makro3.mrk", ButtonMakro3->Caption);
    ButtonMakro4->Caption = LoadMakroName("makro4.mrk", ButtonMakro4->Caption);
    ButtonMakro5->Caption = LoadMakroName("makro5.mrk", ButtonMakro5->Caption);
    ButtonMakro6->Caption = LoadMakroName("makro6.mrk", ButtonMakro6->Caption);
    ButtonMakro7->Caption = LoadMakroName("makro7.mrk", ButtonMakro7->Caption);
    ButtonMakro8->Caption = LoadMakroName("makro8.mrk", ButtonMakro8->Caption);
    ButtonMakro9->Caption = LoadMakroName("makro9.mrk", ButtonMakro9->Caption);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
    writeParamFile(ChangeFileExt( Application->ExeName, ".ini" ));
}
//---------------------------------------------------------------------------

// zisti aktalnu verziu exe
AnsiString TForm1::MyGetApplicationVersion(void)
{
    AnsiString version ="";
    DWORD lpdwHandle;
    char *cisVer;
    UINT iLen = 0;
    char *pBuf;

    iLen = GetFileVersionInfoSize(Application->ExeName.c_str(), &lpdwHandle);
    pBuf = (char *) malloc(iLen);
    GetFileVersionInfo(Application->ExeName.c_str(), 0, iLen, pBuf);

    // "CompanyName", "FileDescription", "FileVersion", "InternalName", "LegalCopyright",
    // "LegalTradeMarks", "OriginalFileName", "ProductName", "ProductVersion", "Comments"
    VerQueryValue(pBuf, "\\StringFileInfo\\040504E2\\FileVersion", (void**)&cisVer, &iLen);
    if(iLen)
       version = cisVer;

    free(pBuf);
    return version;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LabelCopyrightClick(TObject *Sender)
{
    SHELLEXECUTEINFO sei;

    ZeroMemory(&sei,sizeof(sei));
    sei.cbSize    = sizeof(sei);
    sei.fMask     = SEE_MASK_NOCLOSEPROCESS;
    sei.hwnd      = Application->Handle;
    sei.lpVerb    = "open";
    sei.lpFile    = LabelCopyright->Hint.c_str();
    sei.nShow     = SW_SHOW;
    sei.lpParameters  = "";
    sei.lpDirectory   = "";

    ShellExecuteEx(&sei);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditIPPortChange(TObject *Sender)
{
    ServerSocket->Active = false;
    ServerSocket->Port = EditIPPort->Text.ToInt();
    ServerSocket->Active = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormShow(TObject *Sender)
{
    if (TrayIcon->Visible)
        this->MinimalizeClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBoxMinimalizeClick(TObject *Sender)
{
    TrayIcon->Visible = CheckBoxMinimalize->Checked;
    if (TrayIcon->Visible)
        TrayIcon->Minimize();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::OpenClick(TObject *Sender)
{
    TrayIcon->Restore();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MinimalizeClick(TObject *Sender)
{
    TrayIcon->Minimize();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ExitClick(TObject *Sender)
{
    this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MenuItemSelectAllClick(TObject *Sender)
{
    Log->SelectAll();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MenuItemDeleteClick(TObject *Sender)
{
    Log->ClearSelection();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MenuItemCopyClick(TObject *Sender)
{
    Log->CopyToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrayIconBlink()
{
    if( TrayIcon->IconIndex >= ( TrayIcon->Icons->Count - 1))
        TrayIcon->IconIndex = 0;
    else
        TrayIcon->IconIndex++;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBoxCounterClick(TObject *Sender)
{
    EditBackFlushCounter->Visible = CheckBoxCounter->Checked;
    EditScrapCounter->Visible = CheckBoxCounter->Checked;
}
//---------------------------------------------------------------------------

// rozparsuje konfiguracny subor
void TForm1::readParamFile(AnsiString file_name)
{
TIniFile *AppINI = new TIniFile(file_name);
TListBox *listBox;

    listBox = new TListBox(this);
    listBox->Parent = this;
    listBox->Visible = false;

    if (AppINI->ReadInteger( "Form", "LastPosition", 0) == 1)
    {
        Top     = AppINI->ReadInteger( "Form", "Top", Top );
        Left    = AppINI->ReadInteger( "Form", "Left", Left );
        Width   = AppINI->ReadInteger( "Form", "Width", Width );
        Height  = AppINI->ReadInteger( "Form", "Height", Height );
        ValueListEditor->Height = AppINI->ReadInteger( "Form", "Splitter", ValueListEditor->Height );
    }

    CheckBoxMinimalize->Checked = AppINI->ReadBool("Main", "Minimalize", false);
    CheckBoxCounter->Checked = AppINI->ReadBool("Main", "Counter", false);
    EditIPPort->Text = AppINI->ReadString("Main", "IPPort", 502);
    EditBackFlush->Text = AppINI->ReadString("Main", "BackFlush", 0);
    EditScrap->Text = AppINI->ReadString("Main", "Scrap", 0);
    EditBreakDown->Text = AppINI->ReadString("Main", "BreakDown", 0);
    EditBackFlushCounter->Text = AppINI->ReadString("Main", "BackFlushCounter", 0);
    EditScrapCounter->Text = AppINI->ReadString("Main", "ScrapCounter", 0);

    AppINI->ReadSectionValues("Registers", listBox->Items);

    if (listBox->Count > 0 )
        ValueListEditor->Strings->Clear();

    for(int row=0; row < listBox->Count; row++)
    {
        ValueListEditor->Strings->Add(listBox->Items->Strings[row]);
    }

    delete listBox;
    delete AppINI;
}
//---------------------------------------------------------------------------

// pred uzavretim zapise do konfiguracneho suboru
void TForm1::writeParamFile(AnsiString file_name)
{
TIniFile *AppINI = new TIniFile(file_name);
AnsiString key;

    AppINI->WriteInteger( "Form", "Top", Top );
    AppINI->WriteInteger( "Form", "Left", Left );
    AppINI->WriteInteger( "Form", "Width", Width );
    AppINI->WriteInteger( "Form", "Height", Height );
    AppINI->WriteInteger( "Form", "Splitter", ValueListEditor->Height);

    AppINI->WriteBool("Main", "Minimalize", CheckBoxMinimalize->Checked);
    AppINI->WriteBool("Main", "Counter", CheckBoxCounter->Checked);
    AppINI->WriteString("Main", "IPPort", EditIPPort->Text);
    AppINI->WriteString("Main", "BackFlush", EditBackFlush->Text);
    AppINI->WriteString("Main", "Scrap", EditScrap->Text);
    AppINI->WriteString("Main", "BreakDown", EditBreakDown->Text);
    AppINI->WriteString("Main", "BackFlushCounter", EditBackFlushCounter->Text);
    AppINI->WriteString("Main", "ScrapCounter", EditScrapCounter->Text);

    for(int row=0; row < ValueListEditor->RowCount-1; row++)
    {
        try
        {
            key = ValueListEditor->Keys[row+1];
            AppINI->WriteString("Registers", key, ValueListEditor->Values[key]);
        }
        catch (...)
        {
        }
    }
    delete AppINI;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LogChange(TObject *Sender)
{
    if (Log->Lines->Count > MAX_LOG_LINES)
        Log->Clear();
}
//---------------------------------------------------------------------------

// prevod casu na UNIX format
unsigned long UNIXTime (unsigned short year, unsigned short month, unsigned short day, unsigned short hour, unsigned short minute, unsigned short sec)
 {
    unsigned long cislo;
    unsigned char navic;
    day--;
    month--;
    cislo = sec + (unsigned long)minute*SEC_PER_MIN + (unsigned long)hour*SEC_PER_HOUR
         + ((unsigned long)day + (unsigned long)C_MONTH[month]) * SEC_PER_DAY + (unsigned long)year * SEC_PER_DAY * 365;
    if (!( year % 4)) {
        navic = 0;
        if (month>1) { // od brezna pripocte den - 29. unor
            navic = 1;
        }
        navic += (year / 4);
    }
    else
    {
        navic = (year / 4) + 1;
    }
    cislo += (unsigned long)navic * SEC_PER_DAY;
    cislo += (unsigned long)DATEOFFSET1970;
    return cislo;
 }
//---------------------------------------------------------------------------

void __fastcall TForm1::UpdateModbusDateTime()
{
    unsigned short year;
    unsigned short month;
    unsigned short day;
    unsigned short hour;
    unsigned short minute;
    unsigned short sec;
    unsigned short msec;
    unsigned long unix;

    // update time
    TDateTime::CurrentDateTime().DecodeDate(&year, &month, &day);
    TDateTime::CurrentDateTime().DecodeTime(&hour, &minute, &sec, &msec);

    ValueListEditor->Values[ModbusFindInd(44127)] = year;
    ValueListEditor->Values[ModbusFindInd(44125)] = month;
    ValueListEditor->Values[ModbusFindInd(44123)] = day;
    ValueListEditor->Values[ModbusFindInd(44121)] = hour;
    ValueListEditor->Values[ModbusFindInd(44119)] = minute;
    ValueListEditor->Values[ModbusFindInd(44117)] = sec;

    unix = UNIXTime(year - 2000, month, day, hour, minute, sec);
    ValueListEditor->Values[ModbusFindInd(44197)] = unix % 0x10000;
    ValueListEditor->Values[ModbusFindInd(44198)] = unix / 0x10000;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerTCPTimer(TObject *Sender)
{
    if ( !ServerSocket->Socket->ActiveConnections )
        TrayIcon->IconIndex = 1;

    UpdateModbusDateTime();
}
//---------------------------------------------------------------------------

AnsiString __fastcall TForm1::StreamToHex(unsigned char packet[], unsigned short len)
{
    AnsiString line = "";

    for(unsigned short i=0; i<len; i++)
    {
        line += IntToHex(packet[i], 2);
        if(i <= len)
            line += " ";
    }
    return line;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TForm1::ModbusFindInd(unsigned long ind)
{
    AnsiString key;
    AnsiString inds = IntToStr(ind);

    for(unsigned short row=0; row < ValueListEditor->RowCount-1; row++)
    {
        key = ValueListEditor->Keys[row+1];
        if ( key.Pos(inds))
            return key;
    }

//    FileLog->WriteLog(gpacket, 1);
    // nenasiel som register vytvorim ho
    ValueListEditor->Strings->Add(inds + "=0");
    return inds;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketAccept(TObject *Sender,
      TCustomWinSocket *Socket)
{
    Log->Lines->Add("Accept " + Socket->RemoteAddress);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketClientConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
    Log->Lines->Add("ClientConnect");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketClientDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
    Log->Lines->Add("ClientDisconnect");
    TrayIcon->IconIndex = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
    Log->Lines->Add("ClientError: " + IntToStr(ErrorCode));
    ErrorCode = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketGetSocket(TObject *Sender, int Socket,
      TServerClientWinSocket *&ClientSocket)
{
    Log->Lines->Add("GetSocket " + IntToStr(Socket));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketGetThread(TObject *Sender,
      TServerClientWinSocket *ClientSocket,
      TServerClientThread *&SocketThread)
{
    Log->Lines->Add("GetThread");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketThreadEnd(TObject *Sender,
      TServerClientThread *Thread)
{
    Log->Lines->Add("ThreadEnd");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketThreadStart(TObject *Sender,
      TServerClientThread *Thread)
{
    Log->Lines->Add("ThreadStart");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketListen(TObject *Sender,
      TCustomWinSocket *Socket)
{
    Log->Lines->Add("Listen");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketClientWrite(TObject *Sender,
      TCustomWinSocket *Socket)
{
    Log->Lines->Add("ClientWrite");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ServerSocketClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
    AnsiString income;
    unsigned char packet[MAX_TCP_PACKET_LEN];
    int len;

    try
    {
        len = min(sizeof(packet), Socket->ReceiveLength());

        Socket->ReceiveBuf(packet, len);
        TrayIconBlink();

        // spracuj
        income = StreamToHex(packet, len);
        gpacket = income;
        len = MakeAnswer(packet, len);
        Socket->SendBuf(packet, len);
        Log->Lines->Add("Read: " + income);
        Log->Lines->Add("Write: " + StreamToHex(packet, len));
    }
    catch (...)
    {
        Log->Lines->Add("Error: during make answer");
    }
}
//---------------------------------------------------------------------------

unsigned short __fastcall TForm1::MakeAnswer(unsigned char packet[], unsigned short len)
{
    TCP_MODBUS *mbtcp = (TCP_MODBUS*) packet;
    unsigned short ret_len;

    // big swap to PC format
    mbtcp->TransID  = SWAP_SHORT(mbtcp->TransID);
    mbtcp->Protocol = SWAP_SHORT(mbtcp->Protocol);
    mbtcp->Length   = SWAP_SHORT(mbtcp->Length);
    mbtcp->Address  = SWAP_SHORT(mbtcp->Address);
    mbtcp->Quantity = SWAP_SHORT(mbtcp->Quantity);

    if (mbtcp->Length != (unsigned short)(len - HEAD_LEN(mbtcp)))
        Log->Lines->Add("Error packet: wrong length");

    switch (mbtcp->Funct)
    {
        case MODBUS_READ_COIL:
            Log->Lines->Add("Modbus: READ_COIL " + IntToStr(mbtcp->Address + MODBUS_COIL_OFFSET) + ":" + IntToStr(mbtcp->Quantity));
            mbtcp->Length = ModbusReadCoil( mbtcp->Address + MODBUS_COIL_OFFSET, mbtcp->Quantity, (unsigned char*) &mbtcp->Address)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            break;
        case MODBUS_READ_HOLD_REG:
            Log->Lines->Add("Modbus: READ_HOLD_REG " + IntToStr(mbtcp->Address + MODBUS_HOLDING_OFFSET) + ":" + IntToStr(mbtcp->Quantity));
            mbtcp->Length = ModbusReadRegister( mbtcp->Address + MODBUS_HOLDING_OFFSET, mbtcp->Quantity, (unsigned char*) &mbtcp->Address)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            break;
        case MODBUS_READ_DISCRETE:
            Log->Lines->Add("Modbus: READ_INPUT " + IntToStr(mbtcp->Address + MODBUS_DISCRETE_OFFSET) + ":" + IntToStr(mbtcp->Quantity));
            mbtcp->Length = ModbusReadCoil( mbtcp->Address + MODBUS_DISCRETE_OFFSET, mbtcp->Quantity, (unsigned char*) &mbtcp->Address)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            break;
        case MODBUS_READ_INPUT_REG:
            Log->Lines->Add("Modbus: READ_INPUT_REG " + IntToStr(mbtcp->Address + MODBUS_INPUT_OFFSET) + ":" + IntToStr(mbtcp->Quantity));
            mbtcp->Length = ModbusReadRegister( mbtcp->Address + MODBUS_INPUT_OFFSET, mbtcp->Quantity, (unsigned char*) &mbtcp->Address)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            break;
        case MODBUS_FORCE_COIL:
            Log->Lines->Add("Modbus: FORCE_COIL " + IntToStr(mbtcp->Address + MODBUS_COIL_OFFSET));
            mbtcp->Quantity = SWAP_SHORT(mbtcp->Quantity);
            mbtcp->Length = ModbusForceCoil( mbtcp->Address + MODBUS_COIL_OFFSET, mbtcp->Quantity)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            mbtcp->Address = SWAP_SHORT(mbtcp->Address);
            break;
        case MODBUS_WRITE_REGISTER:
            Log->Lines->Add("Modbus: WRITE_REGISTER " + IntToStr(mbtcp->Address + MODBUS_HOLDING_OFFSET) + ":" + IntToStr(mbtcp->Quantity));
            mbtcp->Length = ModbusWriteRegister( mbtcp->Address + MODBUS_HOLDING_OFFSET, mbtcp->Quantity, mbtcp->Data)
                          + sizeof(TCP_MODBUS().Unit)
                          + sizeof(TCP_MODBUS().Funct);
            mbtcp->Address  = SWAP_SHORT(mbtcp->Address);
            mbtcp->Quantity = SWAP_SHORT(mbtcp->Quantity);
            break;
        default:
            Log->Lines->Add("Modbus: NOT SUPPORTED");
            break;
    }

    // big swap to PLC format
    ret_len = mbtcp->Length + HEAD_LEN(mbtcp);
    mbtcp->TransID  = SWAP_SHORT(mbtcp->TransID);
    mbtcp->Protocol = SWAP_SHORT(mbtcp->Protocol);
    mbtcp->Length   = SWAP_SHORT(mbtcp->Length);

    return ret_len;
}
//---------------------------------------------------------------------------

// Example 1:
// 3b7900000006010110030001
// 3b790000000401010100
unsigned short __fastcall TForm1::ModbusReadCoil(unsigned long Address, unsigned short Quantity, unsigned char packet[])
{
    unsigned short ind;
    unsigned short ipacket;
    AnsiString key;
    AnsiString value;

    packet[0] = 0;
    ipacket = 1;

    for(int i=0; i < Quantity; i++)
    {
        key = ModbusFindInd(Address + i);
        value = ValueListEditor->Values[key];

        packet[0] += 1;
        packet[ipacket++] = value.ToIntDef(0) % 0x100;
    }

    return ipacket;
}
//---------------------------------------------------------------------------

// Example 3:
// 3b71000000060103101a0001
// 3b71000000050103020000
unsigned short __fastcall TForm1::ModbusReadRegister(unsigned long Address, unsigned short Quantity, unsigned char packet[])
{
    unsigned short ind;
    unsigned short ipacket;
    AnsiString key;
    AnsiString value;

    packet[0] = 0;
    ipacket = 1;

    for(int i=0; i < Quantity; i++)
    {
        key = ModbusFindInd(Address + i);
        value = ValueListEditor->Values[key];

        packet[0] += 2;
        packet[ipacket++] = value.ToIntDef(0) / 0x100;
        packet[ipacket++] = value.ToIntDef(0) % 0x100;
    }

    return ipacket;
}
//---------------------------------------------------------------------------

// Example 5:
// 00080000000601051001FF00 (on)
// 00080000000601051001FF00
// 000800000006010510010000 (off)
// 000800000006010510010000
unsigned short __fastcall TForm1::ModbusForceCoil(unsigned long Address, unsigned short Coil)
{
    AnsiString key;
    AnsiString value;

    key = ModbusFindInd(Address);
    ValueListEditor->Values[key] = Coil;

    return sizeof(TCP_MODBUS().Address)
         + sizeof(TCP_MODBUS().Quantity);
}
//---------------------------------------------------------------------------

// Example 16:
// 3b7a0000000b0110164800020400010000
// 3b7a00000006011016480002
unsigned short __fastcall TForm1::ModbusWriteRegister(unsigned long Address, unsigned short Quantity, unsigned char packet[])
{
    unsigned short ind;
    unsigned short ipacket;
    AnsiString key;
    AnsiString value;

    ipacket = 1;
    for(int i=0; i < Quantity; i++)
    {
        key = ModbusFindInd(Address + i);
        value = IntToStr(packet[ipacket++] * 0x100 + packet[ipacket++]);
        ValueListEditor->Values[key] = value;
    }

    return sizeof(TCP_MODBUS().Address)
         + sizeof(TCP_MODBUS().Quantity);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditBackFlushChange(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    TimerBackFlush->Enabled = false;
    TimerBackFlush->Interval = EditBackFlush->Text.ToIntDef(0);

    if( TimerBackFlush->Interval == 0 )
        return;

    TimerBackFlush->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerBackFlushTimer(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    if( ValueListEditor->Values[ModbusFindInd(4877)].ToIntDef(0) == 0 )
    {
        if( CheckBoxCounter->Checked )
        {
            if( EditBackFlushCounter->Text.ToIntDef(0) == 0 )
                return;

            EditBackFlushCounter->Text = EditBackFlushCounter->Text.ToIntDef(1) - 1;
        }

        ValueListEditor->Values[ModbusFindInd(44397)] = ValueListEditor->Values[ModbusFindInd(44397)].ToIntDef(0) + 1;
        ValueListEditor->Values[ModbusFindInd(4877)] = 1;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditScrapChange(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    TimerScrap->Enabled = false;
    TimerScrap->Interval = EditScrap->Text.ToIntDef(0);

    if( TimerScrap->Interval == 0 )
        return;

    TimerScrap->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerScrapTimer(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    if( ValueListEditor->Values[ModbusFindInd(4878)].ToIntDef(0) == 0 )
    {
        if( CheckBoxCounter->Checked )
        {
            if( EditScrapCounter->Text.ToIntDef(0) == 0 )
                return;

            EditScrapCounter->Text = EditScrapCounter->Text.ToIntDef(1) - 1;
        }

        ValueListEditor->Values[ModbusFindInd(44887)] = ValueListEditor->Values[ModbusFindInd(44887)].ToIntDef(0) + 1;
        ValueListEditor->Values[ModbusFindInd(4877)] = 1;
        ValueListEditor->Values[ModbusFindInd(4878)] = 1;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditBreakDownChange(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    TimerBreakDown->Enabled = false;

    if( EditBreakDown->Text.ToIntDef(0) > 0 )
        TimerBreakDown->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerBreakDownTimer(TObject *Sender)
{
    if (ButtonFreeze->Tag)
        return;

    if( EditBreakDown->Text.ToIntDef(0) > 0 )
        EditBreakDown->Text = EditBreakDown->Text.ToIntDef(1) - 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonMakro1Click(TObject *Sender) { LoadToList("makro1.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro2Click(TObject *Sender) { LoadToList("makro2.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro3Click(TObject *Sender) { LoadToList("makro3.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro4Click(TObject *Sender) { LoadToList("makro4.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro5Click(TObject *Sender) { LoadToList("makro5.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro6Click(TObject *Sender) { LoadToList("makro6.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro7Click(TObject *Sender) { LoadToList("makro7.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro8Click(TObject *Sender) { LoadToList("makro8.mrk"); DoMakro(); }
void __fastcall TForm1::ButtonMakro9Click(TObject *Sender) { LoadToList("makro9.mrk"); DoMakro(); }
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonFreezeClick(TObject *Sender)
{
    ButtonFreeze->Caption = ButtonFreeze->Tag? "&Freeze" : "Un&Freeze";
    ButtonFreeze->Tag = ButtonFreeze->Tag? 0: 1;

    if (ButtonFreeze->Tag == 0)
    {
        this->EditBackFlushChange(Sender);
        this->EditScrapChange(Sender);
        this->EditBreakDownChange(Sender);
    }
}
//---------------------------------------------------------------------------

enumCommand __fastcall TForm1::GetEnumCommand(AnsiString line)
{
    line = line.UpperCase();

    if (line == "SET") return CMD_SET;
    if (line == "GET") return CMD_GET;
    if (line == "INC") return CMD_INC;
    if (line == "STOP") return CMD_STOP;
    if (line == "START") return CMD_START;
    if (line == "SET_PERIOD") return CMD_SET_PERIOD;
    if (line == "SET_COUNT") return CMD_SET_COUNT;
    if (line == "STOP_COUNT") return CMD_STOP_COUNT;
    if (line == "MAKRO") return CMD_MAKRO;
    if (line == "WAIT") return CMD_WAIT;
    if (line == "EXIT") return CMD_EXIT;

    return CMD_UNKNOWN;
}
//---------------------------------------------------------------------------

enumPeriod __fastcall TForm1::GetEnumPeriod(AnsiString line)
{
    line = line.UpperCase();

    if (line == "BACK_FLUSH") return PRD_BACK_FLUSH;
    if (line == "SCRAP")      return PRD_SCRAP;
    if (line == "BREAK_DOWN") return PRD_BREAK_DOWN;

    return PRD_UNKNOWN;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TForm1::LoadMakroName(AnsiString FileName, AnsiString default_name)
{
    char line_char[MAX_STRING_LEN];
    AnsiString line;

    ifstream is(FileName.c_str());
    if( is.is_open())
    {
        if(!is.eof())
        {
            is.getline(line_char, sizeof line_char,'\n' );
            line = AnsiString(line_char).Trim();
            if (line.Length() > 2)
            if (line[1] == '/')
            {
                default_name = line.SubString(3, line.Length() - 2);
            }
        }
        is.close();
    }

    return default_name;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TForm1::GetCSVValue(AnsiString *line)
{
    AnsiString value = *line;
    int poz;
    if ((poz = line->Pos(CSV_SEPARATOR)) > 0)
    {
        value = line->SubString(1, poz-1);
        *line = line->SubString(poz + 1, line->Length() - poz);
    }
    else
    {
        value = *line;
        *line = "";
    }
    if( value.SubString(1,1).UpperCase() == "R")
    {
        value = ModbusFindInd( StrToInt(value.SubString(2, value.Length())));
        value = ValueListEditor->Values[value].c_str();
    }
    return value;
}
//---------------------------------------------------------------------------

// interactive mode for testing
void __fastcall TForm1::TimerCommandsTimer(TObject *Sender)
{
    if (!FileExists(FILE_COMMAND))
        return;

    if (SecondsBetween(TDateTime::CurrentTime(), FileAge(FILE_COMMAND)) < 3)
        return;

    LoadToList(FILE_COMMAND);
    DoMakro();
    DeleteFile(FILE_COMMAND);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerMakroTimer(TObject *Sender)
{
    TimerMakro->Enabled = false;
    TimerMakro->Interval = 0;
    DoMakro();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LoadToList(char* FileName)
{
    char line_char[MAX_STRING_LEN];

    commands->Clear();

    ifstream is(FileName);
    if( is.is_open())
    {
        while(!is.eof())
        {
            is.getline(line_char, sizeof line_char,'\n' );
            commands->Add( AnsiString(line_char).Trim());
        }
        is.close();
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DoMakro()
{
    while (commands->Count)
    {
        DoCommand(commands->Strings[0]);
        commands->Delete(0);

        // prikaz wait
        if( TimerMakro->Interval != 0 )
        {
            TimerMakro->Enabled = true;
            break;
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DoCommand(AnsiString line)
{
    AnsiString key;
    AnsiString value;

    Log->Lines->Add("Makro " + line);
    switch( GetEnumCommand(GetCSVValue(&line)))
    {
        case CMD_SET:
            key = ModbusFindInd( StrToInt(GetCSVValue(&line)));
            value = GetCSVValue(&line);
            ValueListEditor->Values[key] = value;
            break;
        case CMD_GET:
            key = ModbusFindInd( StrToInt(GetCSVValue(&line)));
            {
                ofstream os(FILE_OUTPUT);
                if( os.is_open())
                {
                    os << key.c_str() << CSV_SEPARATOR;
                    os << ValueListEditor->Values[key].c_str() << "\r\n";
                }
                os.close();
            }
            break;
        case CMD_INC:
            key = ModbusFindInd( StrToInt(GetCSVValue(&line)));
            ValueListEditor->Values[key] = IntToStr(StrToInt(ValueListEditor->Values[key]) + 1);
            break;
        case CMD_STOP:
            ServerSocket->Active = false;
            break;
        case CMD_START:
            ServerSocket->Active = true;
            break;
        case CMD_SET_PERIOD:
            switch ( GetEnumPeriod(GetCSVValue(&line)))
            {
                case PRD_BACK_FLUSH:
                    EditBackFlush->Text = GetCSVValue(&line);
                    break;
                case PRD_SCRAP:
                    EditScrap->Text = GetCSVValue(&line);
                    break;
                case PRD_BREAK_DOWN:
                    EditBreakDown->Text = GetCSVValue(&line);
                    break;
            }
            break;
        case CMD_SET_COUNT:
            switch ( GetEnumPeriod(GetCSVValue(&line)))
            {
                case PRD_BACK_FLUSH:
                    EditBackFlushCounter->Text = GetCSVValue(&line);
                    CheckBoxCounter->Checked = true;
                    break;
                case PRD_SCRAP:
                    EditScrapCounter->Text = GetCSVValue(&line);
                    CheckBoxCounter->Checked = true;
                    break;
            }
            break;
        case CMD_STOP_COUNT:
            CheckBoxCounter->Checked = false;
            break;
        case CMD_WAIT:
            TimerMakro->Enabled = false;
            TimerMakro->Interval = Max(100, StrToInt(GetCSVValue(&line)));
            break;
        case CMD_MAKRO:
            LoadToList(GetCSVValue(&line).c_str());
            break;
        case CMD_EXIT:
            ServerSocket->Active = false;
            this->Close();
            break;
    }
}
//---------------------------------------------------------------------------

