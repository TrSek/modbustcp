//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("ModbusTCPUnit.cpp", Form1);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
        HANDLE hMutex = CreateMutexA(NULL, FALSE, "Modbus TCP");
        DWORD dwMutexWaitResult = WaitForSingleObject(hMutex, 0);
        if (dwMutexWaitResult != WAIT_OBJECT_0)
        {
            MessageBox(HWND_DESKTOP, (Application->ExeName + " is already running").c_str(), TEXT("Information"), MB_OK | MB_ICONINFORMATION);
            CloseHandle(hMutex);
            return -1;
        }

        Application->Initialize();
        Application->Title = "MOdbus Registry Interactive Automat with K";
        Application->CreateForm(__classid(TForm1), &Form1);
        Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
