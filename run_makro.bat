@echo off
rem --------------------------------------------------------------------------
rem Run ModbusTCP.exe on command line.
rem                                         Software (c) 2019, Zdeno Sekerak
rem --------------------------------------------------------------------------

echo Vypnem ak nahodou bezi
for /F %%i IN ('tasklist /NH /FI "IMAGENAME eq ModbusTCP.exe"') do if %%i neq ModbusTCP.exe goto :RunIt
taskkill /IM ModbusTCP.exe /T
timeout 2

:RunIt
if exist command.mrk del command.mrk
echo Pripravim command + spustim na pozadi
echo SET;44098;255 > command.mrk
echo GET;44098    >> command.mrk
START /B ModbusTCP.exe

echo Cakam na ukoncenie prikazu. Kontrolujem ci uz zmazal command.mrk
:CheckFinish
timeout 1
if exist command.mrk (
    goto CheckFinish
)

echo Vypnem ModbusTCP za 3 [s]
echo WAIT;3000 > command.mrk
echo EXIT     >> command.mrk