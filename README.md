# ModbusTCP

MOdbus Registry Interactive Automat with K.
Fork of ModbusTCP

Program po spustení vytvorí súbor ModbusTCP.ini kde má zoznam preddefinovaných registrov. Práca s programom je intuitívne jasná preto pár poznámok.

- v programe je možné simulovať periódu Backflush/Scrap/BreakDown napísaním času v milisekundách, najmenšia zmysluplná perióda je 300 ms
- pre poslanie konkrétneho počtu je nutné nastaviť counter na tento počet a až potom zvoliť periódu, po nastavení periódy sa spustí odosielanie, vždy sa čaká až si Tartan odoberie kus a až potom sa zníži counter
- program sa minimalizuje do tray
- program automaticky počíta aktuálny čas do potrebných registrov
- pokiaľ sa Tartan opýta na neexistujúci register je tento automaticky pridaný na koniec
- v log okne sa zobrazuje aktuálna MODBUS komunikácia
- online parser pre MODBUS komunikaciu je prístupný na http://www.sekerak.eu/modbus/index.php, je potreba zašktrnúť TCP

***Command line pre ModbusTCP:***<br>
Pre potreby testovania je pridaná možnosť ovládania programu cez makrá. Program každé 3 sekundy kontroluje či sa v jeho adresáry nachádza súbor COMMAND.MRK. Ak áno prečíta ho a vykoná operácie. Ak má vyčítať registre tak výsledok uloží do súboru OUTPUT.MRK.

Podporované príkazy:
- SET - nastaví register na požadovanú hodnotu
- GET - vyčíta register a jeho hodnotu uloží do súboru OUTPUT.MRK
- INC - inkrementuje register
- STOP - zastaví TCP komunikáciu
- START - spustí TCP komunikáciu
- SET_PERIOD - nastaví periódu FLUSH/SCRAP/DOWN
- SET_COUNT - nastaví čítač, koľko má odoslať FLUSH/SCRAP/DOWN
- STOP_COUNT - zastaví counter
- MAKRO - spustí makro
- WAIT - počká definovany počet milisekund
- EXIT - ukončí app

Príklad 1 (nastaví register 44098 na hodnotu 255):
> SET;44098;255<br>

Príklad 2 (odošle 10 scrap s periodou 2.2 sekundy):
> SET_COUNT;SCRAP;10<br>
> SET_PERIOD;SCRAP;2200<br>

Príklad 3 (počká 3 sekundy a program ukončí):
> echo WAIT;3000<br>
> echo EXIT<br>

Príklad 4 (pošle 10 backflush po sekunde, počká 15 sekúnd a spustí sa znova):
> SET_COUNT;BACK_FLUSH;10<br>
> SET_PERIOD;BACK_FLUSH;1000<br>
> WAIT;15000<br>
> MAKRO;makro4.mrk<br>

Príklad 5 (do registra 44598 zapíše obsah registra 44598)
> SET;44598;R45698

Poznámky:
- makro súbory a command súbor majú rovnakú syntax
- makro može volať iné makro
- ak sa v makro súbore na prvom riadku nachádza komentár tento je potom zobrazený v názve buttonu programu
- počet riadkov makra je neomedzený
- volanie makra znamená že sa program prepne do nového makra, spať sa už nevráti